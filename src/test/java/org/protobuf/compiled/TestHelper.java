package org.protobuf.compiled;

import java.util.Arrays;
import java.util.List;

public class TestHelper {
    public static final  List<String> WEB_ADDRESS_LIST = Arrays.asList(
            "https://www.youtube.com",
            "https://www.google.com/",
            "https://www.netflix.com/ua-ru/",
            "https://stackoverflow.com/",
            "https://rozetka.com.ua/",
            "https://www.javatpoint.com/",
            "https://jsehelper.blogspot.com/p/welcome.html",
            "https://spring.io",
            "https://rextester.com",
            "https://www.opennet.ru/docs/",
            "https://www.geeksforgeeks.org/",
            "https://dzone.com/",
            "https://springframework.guru/",
            "https://www.microsoft.com/ru-ru",
            "https://www.ibm.com/ua-en",
            "https://www.cisco.com/",
            "https://www.ebay.com/",
            "https://ek.ua/",
            "https://www.i.ua/",
            "https://price.ua/");
}
