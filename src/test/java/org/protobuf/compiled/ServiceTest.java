package org.protobuf.compiled;


import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.ManagedChannel;
import io.grpc.Server;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.NettyServerBuilder;
import org.grpc.client.TestServiceImpl;
import org.junit.jupiter.api.*;

import javax.annotation.Nonnull;
import java.io.IOException;

import static org.protobuf.compiled.TestHelper.WEB_ADDRESS_LIST;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ServiceTest {

    private Server grpcServer;

    @BeforeAll
    void init() throws IOException {
        grpcServer = NettyServerBuilder.forPort(8090)
                .addService(new TestServiceImpl())
                .build();
        grpcServer.start();
    }


    @Test
    public void testSingleBlocking() {
        System.out.println("\nSingle-thread blocking (sync) call test:");

        ManagedChannel grpcChannel = NettyChannelBuilder.forAddress("localhost", 8090).usePlaintext().build();
        TestServerServiceGrpc.TestServerServiceBlockingStub clientBlockingStub = TestServerServiceGrpc.newBlockingStub(grpcChannel);

        MaxExecutionTimeWrapper maxExecutionTime = new MaxExecutionTimeWrapper();
        WEB_ADDRESS_LIST.forEach(webAddress -> {
            RpcResponse rpcResponse = clientBlockingStub.executeSync(getRequest(webAddress));
            System.out.println("Single-thread blocking (sync) response data: " +
                    rpcResponse.getResponseCode() +
                    ":" + rpcResponse.getResponseTime() +
                    ":" + rpcResponse.getWebAddress());
            if (maxExecutionTime.getValue() == null){
                maxExecutionTime.setValue(rpcResponse.getResponseTime());
            } else {
                if (maxExecutionTime.getValue() < rpcResponse.getResponseTime()){
                    maxExecutionTime.setValue(rpcResponse.getResponseTime());
                }
            }
        });
        System.out.println("Max execution time: " + maxExecutionTime.getValue());
    }

    @SuppressWarnings("UnstableApiUsage")
    @Test
    public void testSingleNonBlocking() throws InterruptedException {
        System.out.println("\nSingle-thread non-blocking (sync) call test:");

        ManagedChannel grpcChannel = NettyChannelBuilder.forAddress("localhost", 8090).usePlaintext().build();
        TestServerServiceGrpc.TestServerServiceFutureStub clientFutureStub = TestServerServiceGrpc.newFutureStub(grpcChannel);

        long timeBeforeAllRequests = System.nanoTime()/1000000;
        MaxExecutionTimeWrapper maxExecutionTime = new MaxExecutionTimeWrapper();
        WEB_ADDRESS_LIST.forEach(webAddress -> {
            ListenableFuture<RpcResponse> rpcResponseListenableFuture = clientFutureStub.executeSync(getRequest(webAddress, timeBeforeAllRequests));
            Futures.addCallback(rpcResponseListenableFuture, new FutureCallback<>() {
                @Override
                public void onSuccess(RpcResponse rpcResponse) {
                    long currentAddressRequestExecutionTime = System.nanoTime() / 1000000 - timeBeforeAllRequests;
                    System.out.println("Single-thread non-blocking (sync) response data: " +
                            rpcResponse.getResponseCode() +
                            ":" + rpcResponse.getResponseTime() +
                            ":" + currentAddressRequestExecutionTime +
                            ":" + rpcResponse.getWebAddress());
                    if (maxExecutionTime.getValue() == null){
                        maxExecutionTime.setValue(currentAddressRequestExecutionTime);
                    } else {
                        if (maxExecutionTime.getValue() < currentAddressRequestExecutionTime){
                            maxExecutionTime.setValue(currentAddressRequestExecutionTime);
                        }
                    }
                }

                @Override
                public void onFailure(@Nonnull Throwable t) {
                    t.printStackTrace();
                }
            });
        });
        Thread.sleep(10000);
        System.out.println("Max execution time: " + maxExecutionTime.getValue());
    }

    private static class MaxExecutionTimeWrapper {
        private Long value;

        public Long getValue() {
            return value;
        }

        public void setValue(Long value) {
            this.value = value;
        }
    }

    @Test
    public void testMultiThreadBlocking() throws InterruptedException {
        System.out.println("\nMulti-thread blocking (sync) call test:");

        ManagedChannel grpcChannel = NettyChannelBuilder.forAddress("localhost", 8090).usePlaintext().build();
        TestServerServiceGrpc.TestServerServiceBlockingStub clientBlockingStub = TestServerServiceGrpc.newBlockingStub(grpcChannel);

        WEB_ADDRESS_LIST.forEach(webAddress ->
                new Thread(() -> {
                    RpcResponse rpcResponse = clientBlockingStub.executeSync(getRequest(webAddress));
                    System.out.println("Multi-thread blocking (sync) response data: " +
                            rpcResponse.getResponseCode() +
                            ":" + rpcResponse.getResponseTime() +
                            ":" + rpcResponse.getWebAddress());
                }).start());
        Thread.sleep(10000);
    }

    @SuppressWarnings("UnstableApiUsage")
    @Test
    public void testMultiThreadNonBlocking() throws InterruptedException {
        System.out.println("\nMulti-thread non-blocking (async) call test:");

        ManagedChannel grpcChannel = NettyChannelBuilder.forAddress("localhost", 8090).usePlaintext().build();
        TestServerServiceGrpc.TestServerServiceFutureStub clientFutureStub = TestServerServiceGrpc.newFutureStub(grpcChannel);

        WEB_ADDRESS_LIST.forEach(webAddress ->
                new Thread(() -> {
            ListenableFuture<RpcResponse> rpcResponseListenableFuture = clientFutureStub.executeAsync(getRequest(webAddress));
            Futures.addCallback(rpcResponseListenableFuture, new FutureCallback<>() {
                @Override
                public void onSuccess(RpcResponse rpcResponse) {
                    System.out.println("Multi-thread non-blocking (async) response data: " +
                            rpcResponse.getResponseCode() +
                            ":" + rpcResponse.getResponseTime() +
                            ":" + rpcResponse.getWebAddress());
                }

                @Override
                public void onFailure(@Nonnull Throwable t) {
                    t.printStackTrace();
                }
            });
        }).start());
        Thread.sleep(10000);
    }

    private static RpcRequest getRequest(String webAddress, Long timeBeforeAllRequests) {
        RpcRequest.Builder builder = RpcRequest.newBuilder()
                .setWebAddress(webAddress);
        if(timeBeforeAllRequests != null){
            builder.setTimeFromTheStart(timeBeforeAllRequests);
        }
        return builder.build();
    }

    private static RpcRequest getRequest(String webAddress){
        return getRequest(webAddress, null);
    }

    @AfterAll
    void resetTestedInstance() {
        grpcServer.shutdown();
    }
}
