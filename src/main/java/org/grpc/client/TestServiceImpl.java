package org.grpc.client;

import io.grpc.stub.StreamObserver;
import org.protobuf.compiled.RpcRequest;
import org.protobuf.compiled.RpcResponse;
import org.protobuf.compiled.TestServerServiceGrpc;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class TestServiceImpl extends TestServerServiceGrpc.TestServerServiceImplBase {

    @Override
    public void executeSync(RpcRequest request, StreamObserver<RpcResponse> responseObserver) {
        HttpClient client = getClient();
        HttpRequest httpRequest;

        try {
            httpRequest = getHttpRequest(request);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
        long startDate = System.nanoTime();

        HttpResponse<String> httpResponse;
        try {
            httpResponse = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return;
        }

        long responseTime = getResponseTime(startDate);
        if (httpResponse != null) {
            RpcResponse rpcResponse = getRpcResponse(httpResponse, responseTime);
            setupObserver(responseObserver, rpcResponse);
        }
    }

    @Override
    public void executeAsync(RpcRequest request, StreamObserver<RpcResponse> responseObserver) {
        HttpClient client = getClient();
        HttpRequest httpRequest;
        try {
            httpRequest = getHttpRequest(request);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
        long startDate = System.nanoTime();
        client
                .sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString())
                .thenApply(httpResponse -> {
                    long responseTime = getResponseTime(startDate);
                    return getRpcResponse(httpResponse, responseTime);
                })
                .thenAccept(rpcResponse -> setupObserver(responseObserver, rpcResponse));
    }

    private void setupObserver(StreamObserver<RpcResponse> responseObserver, RpcResponse rpcResponse) {
        responseObserver.onNext(rpcResponse);
        responseObserver.onCompleted();
    }

    private RpcResponse getRpcResponse(HttpResponse<String> httpResponse, long responseTime) {
        return RpcResponse.newBuilder()
                .setResponseCode(httpResponse.statusCode())
                .setResponseTime(responseTime)
                .setWebAddress(httpResponse.uri().toString())
                .build();
    }

    private long getResponseTime(long startDate) {
        return (System.nanoTime() - startDate) / 1000000;
    }

    private HttpRequest getHttpRequest(RpcRequest request) throws URISyntaxException {
        return HttpRequest.newBuilder()
                .uri(getUri(request))
                .build();
    }

    private URI getUri(RpcRequest request) throws URISyntaxException {
        return new URI(request.getWebAddress());
    }

    private HttpClient getClient() {
        return HttpClient.newHttpClient();
    }
}
